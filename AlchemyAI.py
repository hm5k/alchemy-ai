from collections import OrderedDict
from Gameboard import *
import threading
import Queue
import multiprocessing as mp

# used for arbitrarily terrible score
ARBITRARILY_BAD_SCORE = -1e8
ARBITRARILY_GOOD_SCORE = 1e8
# high score is 1.8 million. Dividing 1.5 million by 42 (number of board
# positions) seems like a safe start for prompting transformations
EMPTY_SCORE = 3500

BEST_MOVE_LIST_MOVE = 0
BEST_MOVE_LIST_SECOND_MOVE_LIST = 1
BEST_SECOND_MOVE_SCORE = 0
BEST_SECOND_MOVE_BOARDSTATE = 1


def process_search_move(initial_boardstate):
    ai = AlchemyAI()
    return ai.search_move(initial_boardstate)


def efficient_move_differentiation(board):
    # drop pieces directly instead of "perform move" to avoid implicit calls
    # to heavy calculations
    board.drop_pieces()

    # check matched groups to see if the move is good
    # argument returns groups of 2 as well, so we can maximize pairings
    list_of_groupings = board.find_neighbor_groups(2)

    # square matched group size to favor larger matches
    score = 0
    for group in list_of_groupings:
        sz = len(group)
        score += (sz * sz)

    return score


def score_boardstate(boardstate):
    game_score = 0

    # count empty spaces and score them, but only if there is sufficient
    # space to move elsewhere
    num_fs = boardstate.count(EMPTY)
    game_score += (EMPTY_SCORE * num_fs)

    return game_score

# create one board rather than one for each call
pps_board = AlchemyBoard()


# this is a second-pass score that measure the mean-squared difference between
# pieces
def msd_board_score(boardstate):
    pps_board.load_boardstate(boardstate)

    gb = pps_board.Gameboard
    score = ARBITRARILY_GOOD_SCORE

    # for each position in the gameboard
    for x, col in enumerate(gb):
        for y, row in enumerate(col):
            # if it's not a blank position
            if row != EMPTY:

                # check every other position to the right or bottom of it
                # this avoids double-counting pieces and obeys boundaries
                for x2, col2 in enumerate(gb):
                    for y2, row2 in enumerate(col2):
                        if row2 != EMPTY:

                            # if the comparison is a position to the right or
                            # the bottom of it
                            if ((x2 < (COLUMNS-1)) and (x2 > x)) \
                            or ((y2 < (ROWS-1)) and (y2 > y)):

                                # calculate a weight to apply to the distance
                                # between pieces
                                x_weight = x2 - x
                                y_weight = y2 - y

                                x_weight *= x_weight
                                y_weight *= y_weight

                                weight = x_weight + y_weight

                                # calculate the mean squared difference
                                msd = (piece_dict[row] + piece_dict[row2])/2
                                msd *= msd

                                # make the msd worth less the farther it is
                                msd /= weight

                                # finally, adjust the score
                                score -= msd

    return score


class AlchemyAI:
    def __init__(self):
        # move_list will be a triple list of all possible piece moves
            # it is a list of a *pair* of coordinates, represented as a list
            # each coordinate is a list of two integers
        self.move_list = []
        self.COLUMNS = 6

        self.num_moves_total = 0
        self.total_deep_search_return = 0


        # horizontal move positions
        # "-1" because horz move positions are two wide
        for idx in range(0, self.COLUMNS-1):
            self.move_list.append([[idx, 1], [idx+1, 1]])

        # vertical move positions
        for idx in range(0, self.COLUMNS):
            self.move_list.append([[idx, 0], [idx, 1]])

        # mirrored positions
        # these have been given their own loops because we can keep mirrored
        # pieces from checking half of their moves by splitting like this
        for idx in range(0, self.COLUMNS-1):
            self.move_list.append([[idx+1, 1], [idx, 1]])


        for idx in range(0, self.COLUMNS):
            self.move_list.append([[idx, 1], [idx, 0]])


        self.piece_pair_list = []
        for piece_1 in xrange(MAXIMUM_PIECE_VALUE):
            for piece_2 in reversed(xrange(MAXIMUM_PIECE_VALUE)):
                # avoids any matching pieces or duplicate pairs
                if piece_1 >= piece_2:
                    break
                self.piece_pair_list.append([piece_1, piece_2])

    def random_move(self):
        # select a random move pair from the list and return it
        move_pair = randint(0, len(self.move_list)-1)
        return self.move_list[move_pair]

    def multiprocess_search_moves(self, initial_boardstate):
            # always want a default move
            # want the computer to start by playing farthest to the right
            # this is a weak heuristic gathered from
            list_len = len(self.move_list)

            # default move pair is "final move" when all moves result in a loss
            best_score = ARBITRARILY_BAD_SCORE
            best_move_pair = list_len - 1       # 0-based

            # board for evaluating our position
            board = AlchemyBoard()
            boardstate_list = []
            new_move_list = []

            # for each mod we'll spawn a thread
            for idx, move in enumerate(self.move_list):
                # initialize board state and first move
                board.load_boardstate(initial_boardstate)
                board.set_current_pieces_pos(move[0], move[1])

                # if we don't lose this move, add it to the possible moves
                if board.perform_move() is True:
                    subsequent_boardstate = board.current_boardstate()
                    boardstate_list.append(subsequent_boardstate)
                    new_move_list.append(move)

            pool_size = len(boardstate_list)

            # if there are moves to process
            if pool_size > 0:
                # create processes for these searches to be split between
                pool = mp.Pool(processes=pool_size)
                process_results = pool.map(process_search_move, boardstate_list)
                pool.close()

                # wait for them to finish
                pool.join()

                # for each result, check it against current scores and save the
                # move if it's the best
                for idx, result in enumerate(process_results):

                    # >= favors right-side moves in
                    if result >= best_score:
                        best_move_pair = idx
                        best_score = result
                return new_move_list[best_move_pair]

            # return estimated best move
            return self.move_list[best_move_pair]

    def multithread_search_moves(self, initial_boardstate):
        # remove a dereference
        move_list = self.move_list
        search_move = self.search_move

        # always want a default move
        list_len = len(move_list)-1

        # default move pair is "final move" when all moves result in a loss
        best_move_pair = list_len
        desperation_move = list_len

        # arbitrarily large negative so that we always find a best move, given
        # that we add negative numbers for heuristics
        best_score = ARBITRARILY_BAD_SCORE

        # board for evaluating our position
        board = AlchemyBoard()

        # list to track moves that are equivilently good
        best_move_list = []

        matching_pieces = False
        if board.current_pieces[0][PIECE_TYPE] == \
           board.current_pieces[1][PIECE_TYPE]:
            matching_pieces = True

        # we check in reverse for the same "right side is better" heuristic
        for idx, move_1 in enumerate(reversed(move_list)):
            if matching_pieces is True:
                if idx > 10:
                    break

            # initialize board state and first move
            board.load_boardstate(initial_boardstate)
            board.set_current_pieces_pos(move_1[0], move_1[1])

            # perform the move and search the moves following it to determine
            # best move
            if board.perform_move() is True:

                # this move doesn't kill us, so keep it around as a last resort
                desperation_move = list_len - idx

                # setup and search the second move set
                current_boardstate = board.current_boardstate()
                best_second_moves = search_move(current_boardstate)

                # update our best score, empty the list out
                # this is added into the list with the following if statement
                list_score = best_second_moves[0][BEST_SECOND_MOVE_SCORE]
                if list_score > best_score:
                    best_score = list_score
                    best_move_list[:] = []

                # add this second move to a list of moves that seem to work
                if list_score == best_score:
                    best_move_list.append([idx, best_second_moves])

                    # default move in the case of move_3 failing somehow
                    best_move_pair = list_len - idx

        # if there's only one best first move, don't bother continuing to search
        best_move_list_len = len(best_move_list)
        if best_move_list_len > 1:

            best_score = ARBITRARILY_BAD_SCORE

            is_efficient = False
            if best_move_list_len > 6:
                is_efficient = True

            mt_search_move = self.multithread_search_move

            # then search deeper to find the nuance between the moves
            for move_1 in best_move_list:
                second_move_list = move_1[BEST_MOVE_LIST_SECOND_MOVE_LIST]
                best_second_score = ARBITRARILY_BAD_SCORE

                pool = []
                queue = Queue.Queue()

                temp_third_total = 0

                # for each second move, search a third move and save the best
                for move_2 in second_move_list:
                    move_thread = threading.Thread(
                        target=mt_search_move,
                        name="{0}".format(idx),
                        args=[move_2[BEST_SECOND_MOVE_BOARDSTATE], is_efficient, queue],
                    )
                    move_thread.start()
                    pool.append(move_thread)

                for thread in pool:
                    thread.join()
                    curr_score = queue.get()

                    if curr_score > best_second_score:
                        best_second_score = curr_score
                        temp_third_total = 1
                    elif curr_score == best_second_score:
                        temp_third_total += 1

                # if that third move shows this second move to be the best
                # assign our first move that leads to it. Yes I know.
                if best_second_score > best_score:
                    best_score = best_second_score
                    best_move_pair = list_len - move_1[BEST_MOVE_LIST_MOVE]

                self.total_deep_search_return += temp_third_total

        # never suicide early. Double check moves here because
        # i'm tired bugs creeping in...
        board.load_boardstate(initial_boardstate)
        board.set_current_pieces_pos(move_list[best_move_pair][0],
                                     move_list[best_move_pair][1])
        if board.perform_move() is False:
            return move_list[desperation_move]

        # return estimated best move
        return move_list[best_move_pair]

    def multithread_search_move(self, initial_boardstate, is_efficient, queue):
        best_score = self.deep_search(initial_boardstate, is_efficient)
        queue.put(best_score)

    # spawns searches for each top-level move for subsequent moves
    def search_moves(self, initial_boardstate):
        # always want a default move
        list_len = len(self.move_list)-1

        # default move pair is "final move" when all moves result in a loss
        best_move_pair = list_len

        # arbitrarily large negative so that we always find a best move, given
        # that we add negative numbers for heuristics
        best_score = ARBITRARILY_BAD_SCORE

        # board for evaluating our position
        board = AlchemyBoard()

        best_move_list = []

        # we check in reverse for the same "right side is better" heuristic
        for idx, move_1 in enumerate(reversed(self.move_list)):

            # initialize board state and first move
            board.load_boardstate(initial_boardstate)
            board.set_current_pieces_pos(move_1[0], move_1[1])

            # perform the move and search the moves following it to determine
            # best move
            if board.perform_move() is True:

                current_boardstate = board.current_boardstate()
                best_second_moves = self.search_move(current_boardstate)

                # update our best score, empty the list out
                # this is added into the list with the following if statement
                list_score = best_second_moves[0][BEST_SECOND_MOVE_SCORE]
                if list_score > best_score:
                    best_score = list_score
                    best_move_list[:] = []

                if list_score == best_score:
                    best_move_list.append([idx, best_second_moves])

                    # !!!***REMOVE THIS LATER***!!!
                    best_move_pair = list_len - idx

        # if there's only one best first move, don't bother continuing to search
        if len(best_move_list) > 1:
            best_score = ARBITRARILY_BAD_SCORE

            # then search deeper to find the nuance between the moves
            for move_1 in best_move_list:
                second_move_list = move_1[BEST_MOVE_LIST_SECOND_MOVE_LIST]
                best_second_score = ARBITRARILY_BAD_SCORE

                # for each second move, search a third move and save the best
                for move_2 in second_move_list:
                    curr_score = self.deep_search(
                        move_2[BEST_SECOND_MOVE_BOARDSTATE])
                    if curr_score > best_second_score:
                        best_second_score = curr_score

                # if that third move shows this second move to be the best
                # assign our first move that leads to it. Yes I know.
                if best_second_score > best_score:
                    best_score = best_second_score
                    best_move_pair = list_len - move_1[BEST_MOVE_LIST_MOVE]

        # return estimated best move
        return self.move_list[best_move_pair]

    def deep_search(self, initial_boardstate, is_efficient):

        # build/load our board
        board = AlchemyBoard()
        board.load_boardstate(initial_boardstate)

        # save on defreferencing objects by assigning them to local objects
        piece_pair_list = self.piece_pair_list
        max_piece = board.max_piece
        search_move = self.search_move

        total_score = 0

        # search each possible move for pieces that could come up
        # in order to play around them
        for piece_pair in piece_pair_list:

            # attempt to save on defreferenceing and add readability
            piece_1 = piece_pair[0]
            piece_2 = piece_pair[1]

            # only deep search pieces we can attain
            if (piece_1 < max_piece) \
            and (piece_2 < max_piece):

                # pessimistic pruning: don't expect easy solves to fall
                # could refine this to be more accurate by checking against
                # top pieces of each column
                if piece_1 != piece_2:
                    if is_efficient is True:
                        # perform an efficient measurement
                        board.load_boardstate(initial_boardstate)
                        board.set_current_pieces_type(piece_1, piece_2)
                        total_score += efficient_move_differentiation(board)

                    else:
                        # load our pieces
                        board.set_current_pieces_type(piece_1, piece_2)

                        # search the move, and essentially* average the results
                        current_boardstate = board.current_boardstate()
                        move_list = search_move(current_boardstate)
                        total_score += move_list[0][BEST_SECOND_MOVE_SCORE]


        return total_score

    # searches subsequent moves to determine most preferential boardstate
    def search_move(self, initial_boardstate):
        # board for evaluating our position
        board = AlchemyBoard()

        # arbitrarily large negative so that we always find a best move, given
        # that we add negative numbers for heuristics
        best_score = ARBITRARILY_BAD_SCORE
        best_boardstate = initial_boardstate
        best_move_list = [[best_score, best_boardstate]]

        # save on dereferencing
        # load_boardstate = board.load_boardstate
        move_list = self.move_list

        # for each move in the list compare their score
        for move_2 in move_list:
            # initialize board state and first move
            board.load_boardstate(initial_boardstate)
            board.set_current_pieces_pos(move_2[0], move_2[1])

            # note: we don't care if it's a loss here, though we might in the
            # future if this becomes recursive
            board.perform_move()

            # save current boardstate
            current_boardstate = board.current_boardstate()

            # get score of current move
            curr_score = score_boardstate(current_boardstate)

            # update our best score, empty the list out
            # this is added into the list with the following if statement
            if curr_score > best_score:
                best_score = curr_score
                best_move_list[:] = []

            # then update the list if we beat or matched the best score
            if curr_score == best_score:
                best_move_list.append([best_score, current_boardstate])

        return best_move_list
