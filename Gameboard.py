from __future__ import print_function
from math import pow
from random import randint

# note used yet. Have to verify accuracy.
MAXIMUM_PIECE_VALUE = 12

# used for marking pieces without storing different data
# or mutating data types
MARKED = 100

# grid description
COLUMNS = 6
ROWS = 7 + 2  # 7 rows, -1 => 0-based

# def for empty squares
EMPTY = 'F'
EMPTY_COLUMN = "FFFFFFFFF"

# macros help to clean up code with missing object orientation
PIECE_TYPE = 1
COORDS = 0
X = 0
Y = 1

piece_dict = {'0': 0, '1': 1, '2': 2, '3': 3, '4': 4, '5': 5, '6': 6,
              '7': 7, '8': 8, '9': 9, 'A': 10, 'B': 11, 'C': 12, 'D': 13,
              0: '0', 1: '1', 2: '2', 3: '3', 4: '4', 5: '5', 6: '6',
              7: '7', 8: '8', 9: '9', 10: 'A', 11: 'B', 12: 'C', 13: 'D',
              'F': 'F'}


class AlchemyBoard:
    def __init__(self):
        self.max_piece = 1

        # build a board of Gamepieces (which initialize as "empty")
        self.Gameboard = []

        for i in xrange(COLUMNS):
            self.Gameboard.append(EMPTY_COLUMN)

        self.preview_pieces = [EMPTY, EMPTY]
        self.current_pieces = []

        # initialize first move
        self.current_pieces.append([[2, 1], randint(0, self.max_piece)])
        self.current_pieces.append([[3, 1], randint(0, self.max_piece)])

        self.generate_preview_pieces()

    # randomly gens the preview pieces, convenience function
    def generate_preview_pieces(self):
        p_pcs = self.preview_pieces

        p_pcs[0] = randint(0, self.max_piece)
        p_pcs[1] = randint(0, self.max_piece)

    # used to place the pieces on the board where you wish
    # will need another version for piece IDs or something
    def set_current_pieces_pos(self, first_piece_pos, second_piece_pos):
        c_pcs = self.current_pieces

        # clear gameboard of old "current pieces"
        c_pcs[0][COORDS] = first_piece_pos
        c_pcs[1][COORDS] = second_piece_pos

    def set_current_pieces_type(self, first_piece_type, second_piece_type):
        c_pcs = self.current_pieces

        c_pcs[0][PIECE_TYPE] = first_piece_type
        c_pcs[1][PIECE_TYPE] = second_piece_type

    # turns a gameboard-piece into a blank piece again
    # helps keep us from having python-reference-only style problems
    def reset_piece(self, coords):
        self.set_piece(EMPTY, coords)

    def set_piece(self, piece_type, coords):
        col = coords[X]
        row = coords[Y]
        gameboard = self.Gameboard

        if row == (ROWS-1):
            gameboard[col] = gameboard[col][:row] \
                                + piece_dict[piece_type]
        else:
            gameboard[col] = gameboard[col][:row] \
                                + piece_dict[piece_type] \
                                + gameboard[col][row+1:]

    # used by the above to drop the piece
    def drop_piece(self, in_piece):
        col = in_piece[COORDS][X]
        row = self.Gameboard[col].rfind(EMPTY)
        if row >= 0:
            self.set_piece(in_piece[PIECE_TYPE], [col, row])

    # drops pieces, updates the board
    def perform_move(self):
        self.drop_pieces()

        # look for and perform transforms
        self.update_grid()

        # loss condition?
        if self.player_lost():
            return False

        # update current pieces and gameboard
        self.current_pieces[0][PIECE_TYPE] = self.preview_pieces[0]
        self.current_pieces[1][PIECE_TYPE] = self.preview_pieces[1]

        # generate new preview pieces
        # MUST COME AFTER update_grid OR YOU COULD MISS A MAXPIECE UPDATE
        self.generate_preview_pieces()

        return True

    # drops each "current piece" onto the board
    def drop_pieces(self):
        # remove some dereferences
        piece_1 = self.current_pieces[0]
        piece_2 = self.current_pieces[1]
        drop_piece = self.drop_piece

        # drop bottom-most piece or first piece first, then the remaining piece
        if piece_1[Y] >= \
                piece_2[Y]:
            drop_piece(piece_1)
            drop_piece(piece_2)
        else:
            drop_piece(piece_2)
            drop_piece(piece_1)

    # used to check if a player has achieved the loss condition
    def player_lost(self):
        gameboard = self.Gameboard
        for col in gameboard:
            if col.rfind(EMPTY) < 1:
                return True
        return False

    # returns score of the current boardstate
    def game_score(self):
        score = 0
        gameboard = self.Gameboard
        for col in gameboard:
            for char in col:
                if char != EMPTY:
                    score += int(pow(3.0, piece_dict[char]))

        return score

    # now the same as game_score, left to maintain interface for now
    def final_score(self):
        return self.game_score()

    # simply saves the game-board to a file
    def save_boardstate_file(self, filename="boardstate.sav"):
        # get boardstate
        boardstate = self.current_boardstate()

        # save to a file
        savefile = open(filename, "w")
        savefile.write(boardstate)
        savefile.close()

        print(boardstate)

    # loads a boardstate of the "save_boardstate" format onto the gameboard
    def load_boardstate_file(self, filename="boardstate.sav"):
        try:
            loadfile = open(filename, "r")
        except IOError:
            print("Could not open {}".format(filename))
            return False

        # load boardstate
        boardstate = loadfile.readline()
        self.load_boardstate(boardstate)

        loadfile.close()
        return True

    # resets the board. Likely unnecessary post-refactoring
    def clear_boardstate(self):
        for col in self.Gameboard:
            col = EMPTY_COLUMN
        self.current_pieces[0][PIECE_TYPE] = 0
        self.current_pieces[1][PIECE_TYPE] = 0
        self.preview_pieces[0] = 0
        self.preview_pieces[1] = 0
        self.max_piece = 1

    # returns a string that represents the current boardstate
    def current_boardstate(self):
        gameboard = self.Gameboard
        c_pcs = self.current_pieces
        p_pcs = self.preview_pieces

        # yes this is ugly. But it is far far faster when it comes to python
        # speed is the whole point of this refactor, better to not cut corners
        boardstate = "{0}{1}{2}{3}{4}{5}{6}{7}{8}{9}{10}".format(
            gameboard[0],
            gameboard[1],
            gameboard[2],
            gameboard[3],
            gameboard[4],
            gameboard[5],
            str(c_pcs[0][PIECE_TYPE]),
            str(c_pcs[1][PIECE_TYPE]),
            str(p_pcs[0]),
            str(p_pcs[1]),
            str(self.max_piece)
        )

        return boardstate

    # loads a boardstate from a string
    # in the format returned by "self.current.boardstate()"
    def load_boardstate(self, boardstate):
        self.clear_boardstate()

        for idx in xrange(6):
            start = idx * 9
            end = ((idx + 1) * 9)
            self.Gameboard[idx] = boardstate[start:end]

        other_info = boardstate[(ROWS * COLUMNS):]
        for idx, char in enumerate(other_info):
            if idx < 2:
                self.current_pieces[idx][PIECE_TYPE] = int(char)
            elif idx < 4:
                self.preview_pieces[idx-2] = int(char)
            elif idx == 4:
                self.max_piece = int(char)

    def find_neighbor_groups(self, min_group=3):
        piece_list = []
        # build a list for each possible piece type to emulate a hash
        for i in xrange(MAXIMUM_PIECE_VALUE):
            piece_list.append([])

        # hash all same-type pieces into their lists
        for idx_x, col in enumerate(self.Gameboard):
            idx_y = 0
            for row in col:
                if row != EMPTY:
                    i_row = piece_dict[row]
                    piece_list[i_row].append([idx_x, idx_y])
                idx_y += 1

        # list of groups
        groups_to_transform = []

        # remove a dereference
        find_neighbors = self.find_neighbors

        # find all groups that need transformation
        for coord_list in piece_list:
            # only check for lists that *could* contain transforms
            if len(coord_list) >= min_group:
                # check the coordinates against eachother to find which
                # are touching, then perform transformations
                while coord_list:
                    coord_group = \
                        find_neighbors(coord_list[0], coord_list, 0)
                    if len(coord_group) >= min_group:
                        groups_to_transform.append(coord_group)

        return groups_to_transform

    # drops pieces to lowest point and readies transformations
    def update_grid(self):
        continue_looping = True

        find_neighbor_groups = self.find_neighbor_groups

        # find all groups of pieces to transform
        while continue_looping is True:
            continue_looping = False
            did_transform = False

            # list of groups to transform comes from the neighbors of pieces
            groups_to_transform = find_neighbor_groups()

            # remove a dereference
            transform = self.transform
            gameboard = self.Gameboard

            # transform those groupings
            for group in groups_to_transform:
                transform(group)
                did_transform = True

            # this section drops pieces when necessary
            if did_transform is True:
                continue_looping = True
                for idx, col in enumerate(gameboard):
                    if col != EMPTY_COLUMN:
                        col_char_str = list(col)

                        num_empties = 0
                        for char in col_char_str:
                            if char == EMPTY:
                                num_empties += 1

                        for i in xrange(num_empties):
                            col_char_str.remove(EMPTY)

                        derk = 9 - len(col_char_str)
                        for i in xrange(derk):
                            col_char_str.insert(0, EMPTY)

                        # finally save the column back as a string
                        gameboard[idx] = ''.join(col_char_str)

        # "easier" version of the game
        # new_max_piece = int(pow(self.game_score(), 1/3))
        # if new_max_piece > self.max_piece:
        #     self.max_piece = new_max_piece

    def find_neighbors(self, coord_1, possible_neighbors, depth):
        # check the coordinates against eachother to find which
        # are touching, then perform transformations
        new_coord_list = []

        if depth == 0:
            possible_neighbors.remove(coord_1)

        for coord_2 in possible_neighbors:
            # diffs reduce dereferences
            # get the absolute values of the diffs
            x_diff = coord_1[X] - coord_2[X]
            if x_diff < 0:
                x_diff = 0 - x_diff

            y_diff = coord_1[Y] - coord_2[Y]
            if y_diff < 0:
                y_diff = 0 - y_diff

            # if the coordinates are touching or the same remove coordinates
            # from the piece_list and add them to our neighbor group
            #
            # NOTE: Will fail a comparison with itself, which is relevant
            # for the first pass
            total_diff = y_diff + x_diff
            if total_diff == 1:
                new_coord_list.append(coord_2)

        # empty all of the possibly redundant coordinates out
        for new_coord in new_coord_list:
            possible_neighbors.remove(new_coord)

        # find all neighbors to our neighbors
        templist = []

        # remove a dereference
        find_neighbors = self.find_neighbors

        for new_coord in new_coord_list:
            if possible_neighbors:
                templist = find_neighbors(new_coord, possible_neighbors, 1)

        # add those neighbors to our group
        for new_coord in templist:
            new_coord_list.append(new_coord)

        if depth == 0:
            new_coord_list.append(coord_1)

        return new_coord_list

    # takes a list of coordinates to determine the appropriate piece to transform
    # it then transforms that piece and resets all other pieces in the list
    def transform(self, group_coordinates):
        bottom_most = 0
        left_most = 6

        # find left-most bottom-most node
        for coords in group_coordinates:
            if (coords[0] < left_most)\
            and (coords[1] == bottom_most):
                left_most = coords[0]
            elif coords[1] > bottom_most:
                bottom_most = coords[1]
                left_most = coords[0]

        # remove a dereference
        gameboard = self.Gameboard
        set_piece = self.set_piece

        # reset each node unless it's the left-most bottom-most node
        for coords in group_coordinates:
            x_coord = coords[0]
            y_coord = coords[1]

            if (x_coord == left_most) \
            and (y_coord == bottom_most):
                p_id = piece_dict[gameboard[x_coord][y_coord]]
                p_id += 1
                if self.max_piece < p_id:
                    self.max_piece = p_id

                set_piece(p_id, coords)

            else:
                set_piece(EMPTY, coords)

    # this is pretty inefficient, but it only gets called once per move, so it's
    # not a big deal yet
    def draw_gameboard(self):
        print ("\n\n-  -  -  -  -  -  -  - - - - -")
        for row in xrange(ROWS):
            line = "|  "
            for idx, col in enumerate(self.Gameboard):
                if [idx, row] == self.current_pieces[0][COORDS]:
                    line += piece_dict[self.current_pieces[0][PIECE_TYPE]]
                elif [idx, row] == self.current_pieces[1][COORDS]:
                    line += piece_dict[self.current_pieces[1][PIECE_TYPE]]
                else:
                    char = self.Gameboard[idx][row]

                    if char != EMPTY:
                        line += self.Gameboard[idx][row]
                    else:
                        line += " "
                line += "  "
            line += "| "

            if row < 2:
                # add the preview pieces
                line += piece_dict[self.preview_pieces[row]]
                line += " |"

                if row == 0:
                    line += "MAX|"
                else:
                    line += " "
                    line += piece_dict[self.max_piece]
                    line += " |"

            elif row == 2:
                print ("|  -  -  -  -  -  -  | - | - |")

            print(line)

        print ("|  -  -  -  -  -  -  |")

        line = "| Score: "
        line += "{0:>{1}}".format(int(self.game_score()), 12)
        line += "|"
        print(line)

    # same as the draw_gameboard but doesn't draw "current pieces"
    # which gives a more accurate representation of the boardstate
    def final_gameboard(self):
        print ("\n\n-  -  -  -  -  -  -  - - - - -")
        for row in xrange(ROWS):
            line = "|  "
            for idx, col in enumerate(self.Gameboard):
                char = self.Gameboard[idx][row]

                if char != EMPTY:
                    line += self.Gameboard[idx][row]
                else:
                    line += " "
                line += "  "
            line += "| "

            if row < 2:
                # add the preview pieces
                line += str(self.preview_pieces[row])
                line += " |"

                if row == 0:
                    line += "MAX|"
                else:
                    line += " "
                    line += str(self.max_piece)
                    line += " |"

            elif row == 2:
                print ("|  -  -  -  -  -  -  | - | - |")

            print(line)

        print ("|  -  -  -  -  -  -  |")

        line = "| Score: "
        line += "{0:>{1}}".format(int(self.game_score()), 12)
        line += "|"
        print(line)