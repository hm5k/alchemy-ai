from math import pow
from random import randint

EMPTY = -1


class AlchemyPiece:
    def __init__(self):
        self.id = EMPTY
        self.marked = False
        self.coordinates = [0, 0]

    def value(self):
        return int(pow(3.0, float(self.id)))

    def random_piece(self, max_piece):
        self.id = randint(0, max_piece)

    def reset_piece(self):
        old_coordinates = self.coordinates
        self.__init__()
        self.coordinates = old_coordinates