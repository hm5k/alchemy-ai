from __future__ import print_function
from Gameboard import AlchemyBoard
from AlchemyAI import AlchemyAI
import time

'''
from email.mime import image
from selenium import webdriver
from selenium.webdriver.support import ui
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
'''


if __name__ == '__main__':
    ai = AlchemyAI()
    board = AlchemyBoard()

    total_score = 0
    high_score = 0
    total_average = 0
    total_game_time = 0
    num_games = 1

    for i in range(0, num_games):
        timer = time.clock()
        board = AlchemyBoard()
        num_moves = 0
        while True:
            num_moves += 1

            # current state is necessary for the AI
            current_boardstate = board.current_boardstate()

            # gather the AI's move
            # move = ai.multiprocess_search_moves(current_boardstate)
            move = ai.multithread_search_moves(current_boardstate)
            # move = ai.search_moves(current_boardstate)

            # perform the move, draw the board, and end the game if necessary
            board.set_current_pieces_pos(move[0], move[1])
            print("Move {0} of Game {1}".format(num_moves, i+1))
            board.draw_gameboard()
            if board.perform_move() is False:
                break
        time.sleep(1)
        timer = time.clock() - timer
        total_game_time += timer

        # draw final boardstate and Game Over message
        board.final_gameboard()
        final_score = board.final_score()

        print("Game {3} Over. Final score is {0} in {1} moves in {2} seconds.".format(final_score, num_moves, timer, i+1))
        total_score += final_score
        if final_score > high_score:
            high_score = final_score

        print("Average return of a Third-Move search is: {0}".format(
            ai.total_deep_search_return/num_moves))
        total_average += ai.total_deep_search_return/num_moves
        time.sleep(2)

    # Some analysis statements to get an idea of how the AI is doing
    # and if more pruning and heuristics will help
    print("\n\nAI played {0} games and yieled the following results:".format(num_games))
    print(" * An average score of {0}".format(total_score/num_games))
    print(" * A high score of {0}".format(high_score))
    print(" * An Average set size of equivalent third-moves "
          "per move: {0}".format(total_average/num_games))
    print(" * Each game took an average of {0} seconds".format(
        total_game_time/num_games))